import argparse
import json
import logging
import sys
from typing import Optional, List

from .action import get_actions, show_actions, perform_actions
from .archiver import get_current_attributes
from .config import load_configuration, get_desired_attributes, get_tango_database

try:
    from ._version import version  # type: ignore
except ImportError:
    version = "unknown"


def main(raw_args: Optional[List[str]] = sys.argv[1:]):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "yamlfile",
        type=str,
        help="A YAML file containing archiver configuration.",
    )
    parser.add_argument(
        "--write",
        "-w",
        default=False,
        help="Apply the changes to the archiver",
        action="store_true",
    )
    parser.add_argument(
        "--update",
        "-u",
        default=False,
        help="Don't remove any attributes, only add or change",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Show debug messages",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--show",
        help="Print out the complete configuration",
        action="store_true",
    )
    parser.add_argument(
        "--delay",
        type=float,
        default=0.3,
        help="Delay in between archiving operations",
    )
    parser.add_argument("-V", "--version", action="version", version=version)

    # Note: raw_args used by tests that call this function programmatically
    args = parser.parse_args(raw_args)

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stderr)
    else:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr)

    try:
        configure(
            args.yamlfile,
            update=args.update,
            write=args.write,
            show=args.show,
            delay=args.delay,
        )
    except RuntimeError as e:
        logging.fatal(f"Error: {e}")
        sys.exit(1)


def configure(yamlfile: str, update: bool, write: bool, show: bool, delay: float = 0):

    config = load_configuration(yamlfile)

    if show:
        print(json.dumps(config, indent=4))
        return

    db = get_tango_database(config["db"])

    # Get the current archiving configuration. This consists of
    # all archiving related settings for each attribute currently
    # archived by the archiver.
    current = get_current_attributes(config["archiver"], delay=delay)

    # Build the configuration described by the config file + reality
    desired = get_desired_attributes(config["db"], config["configuration"], db)

    # Calculate what is needed to bring the current config to the desired state
    actions = get_actions(current, desired)

    if any(actions.values()):
        output = show_actions(actions, update=update)
        if output:
            print("--- Required actions ---")
            print("\n".join(output))
            if write:
                print("--- Applying actions... ---")
                failed = perform_actions(
                    config["manager"],
                    config["archiver"],
                    actions,
                    update=update,
                    delay=delay,
                )
                failed_output = []
                for key, attrs in failed.items():
                    if attrs:
                        failed_output.append(key.upper())
                        for attr, error in attrs:
                            line_error = error.replace("\n", " ")
                            failed_output.append(f"\t{attr}: {line_error}")
                if failed_output:
                    print()
                    print("!!! Actions applied, but the following actions failed !!!")
                    print("\n".join(failed_output))
                else:
                    print(
                        "*** Successfully applied the actions to the archiving system! ***"
                    )
                if update:
                    print(
                        "Warning: since the '-u/--update' flag was used, the archiver may not be"
                    )
                    print("in sync with the YAML file!")
            else:
                print("=== No actions made, try the '--write' option to apply. ===")
        else:
            print("--- Nothing to do! ---")
        # if actions["broken"] or actions["pre_broken"]:
        #     print("!!! Errors may have prevented some actions; check log messages !!!")
    else:
        print("--- Nothing to do! ---")
        return
