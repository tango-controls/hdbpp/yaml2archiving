from itertools import islice
import logging
import sys
from typing import Sequence
from urllib.parse import urlparse

if sys.version_info >= (3, 8):
    from typing import TypedDict
else:
    from typing_extensions import TypedDict

import tango  # type: ignore


ARCHIVING_PARAMS = [
    "archive_abs_change",
    "archive_rel_change",
    "archive_period",
    "polling_period",
    "archive_strategy",
]


AttributeConfig = TypedDict(
    "AttributeConfig",
    {
        "polling_period": int,
        "code_push_event": bool,
        "archive_period": int,
        "archive_rel_change": float,
        "archive_abs_change": float,
        "archive_strategy": str,
        "ttl": int,
    },
    total=False,
)


def get_attribute_config(attr_proxy: tango.AttributeProxy) -> AttributeConfig:
    config = attr_proxy.get_config()
    arch_event = config.events.arch_event
    result = AttributeConfig()
    if arch_event.archive_abs_change != "Not specified":
        result["archive_abs_change"] = float(arch_event.archive_abs_change)
    if arch_event.archive_rel_change != "Not specified":
        result["archive_rel_change"] = float(arch_event.archive_rel_change)
    if arch_event.archive_period != "Not specified":
        result["archive_period"] = int(arch_event.archive_period)
    polling_period = attr_proxy.get_poll_period()
    if polling_period:
        result["polling_period"] = polling_period
    return result


# @lru_cache(0)
# def get_attr_db_config(attribute: str) -> AttributeConfig:
#     proxy = tango.AttributeProxy(attribute)
#     return get_attribute_config(proxy)


def format_tango_exc(exc):
    return exc.args[-1].desc.splitlines()[0]


def nwise(it, n):
    # [s_0, s_1, ...] => [(s_0, ..., s_(n-1)), (s_n, ... s_(2n-1)), ...]
    return list(zip(*[islice(it, i, None, n) for i in range(n)]))


def get_attribute_configs(
    db: tango.Database, archiver_attribute_list: Sequence[str]
) -> dict[str, AttributeConfig]:
    """
    Get current archiving configuration for the given attributes, from the Tango DB
    """
    if not archiver_attribute_list:
        # Nothing to check!
        return {}
    result: dict[str, AttributeConfig] = {
        a: {}  # If there are no settings in the db, default to empty
        for a in archiver_attribute_list
    }
    # Get attribute properties for all involved devices
    device_attrs: dict[str, set[str]] = {}
    full_device_names: dict[str, str] = {}
    for name in result:
        o = urlparse(name.lower())  # We may get the attribute as a full URI
        dev, attr = o.path.lstrip("/").rsplit("/", maxsplit=1)
        full_device_names[dev] = name.rsplit("/", 1)[0]
        device_attrs.setdefault(dev, set()).add(attr)
    device_array = ",".join(f"'{dev}'" for dev in device_attrs.keys())
    _, prop_rows = db.command_inout(
        "DbMySQLSelect",
        f"""
        SELECT device, attribute, name, value
        FROM property_attribute_device
        WHERE device IN ({device_array})
        ORDER BY device, name, count
        """,
    )
    for device, attr, prop, value in nwise(prop_rows, 4):
        device = device.lower()
        attr = attr.lower()
        if attr not in device_attrs[device]:
            continue
        full_device = full_device_names[device]
        settings = result.setdefault(f"{full_device}/{attr}".lower(), {})
        try:
            match prop:
                case "archive_abs_change" | "archive_rel_change":
                    # Note: there may be up to two values; negative and positive change
                    # For now we ignore any negative value. So far I have never
                    # seen a need for using two different values. But we could
                    # support it in the future.
                    settings[prop] = abs(float(value))
                case "archive_period":
                    settings[prop] = int(float(value))
        except ValueError as e:
            logging.error(
                f"Failed to parse attribute property '{prop}' value '{value}' for {full_device}/{attr}: {e}"
            )

    # Get polling settings for all involved devices
    _, polling_rows = db.command_inout(
        "DbMySQLSelect",
        f"""
        SELECT device, value
        FROM property_device
        WHERE name = 'polled_attr' AND device IN ({device_array})
        ORDER BY device, count
        """,
    )
    # polled_attr is a list of attr1, period1, attr1, period2, ...
    for (device, attr), (_, period) in nwise(nwise(polling_rows, 2), 2):
        device = device.lower()
        attr = attr.lower()
        if attr in device_attrs[device]:
            full_device = full_device_names[device]
            try:
                result[f"{full_device}/{attr}".lower()]["polling_period"] = int(
                    float(period)
                )
            except ValueError as e:
                logging.error(
                    f"Failed to parse polling period '{period}'for {full_device}: {e}"
                )
                raise ValueError(
                    f"Could not get polling period from database for {full_device}: {e}"
                )
    return result
