from jsonschema import Draft7Validator  # type: ignore


ATTRIBUTES = {
    "title": "Attribute configuration",
    "type": "object",
    "minProperties": 1,
    "additionalProperties": {
        "type": ["object", "null"],  # Null cancels default
        "additionalProperties": False,
        "minProperties": 1,
        "properties": {
            "polling_period": {
                "description": "Attribute polling period, in milliseconds",
                "type": ["integer", "null"],
                "minimum": 0,
            },
            "code_push_event": {
                "description": "Whether the device will push events by itself",
                # TODO mutually exclusive to polling_period...
                "type": ["boolean", "null"],
            },
            "archive_period": {
                "description": "Minimum archiving period, in milliseconds.",
                "type": ["integer", "null"],
                "minimum": 0,
            },
            "archive_rel_change": {
                "description": (
                    "Minimum relative change," " in percent of the last value"
                ),
                # TODO we should also allow a list of two numbers,
                # negative and positive change
                "type": ["number", "null"],
            },
            "archive_abs_change": {
                "description": (
                    "Minimum absolute change," " in whatever unit the attribute uses."
                ),
                # TODO: DITO
                "type": ["number", "null"],
            },
            "archive_strategy": {
                "type": ["string", "null"],
            },
        },
    },
}


ATTRIBUTE_DEFAULTS = {
    "title": "Attribute configuration",
    "type": "object",
    "minProperties": 1,
    "additionalProperties": {
        "type": "object",
        "additionalProperties": False,
        "minProperties": 1,
        "properties": {
            "polling_period": {
                "description": "Attribute polling period, in milliseconds",
                "type": "integer",
                "minimum": 0,
            },
            "code_push_event": {
                "description": "Whether the device will push events by itself",
                # TODO mutually exclusive to polling_period...
                "type": "boolean",
            },
            "archive_period": {
                "description": "Minimum archiving period, in milliseconds.",
                "type": "integer",
                "minimum": 0,
            },
            "archive_rel_change": {
                "description": (
                    "Minimum relative change," " in percent of the last value"
                ),
                # TODO we should also allow a list of two numbers,
                # negative and positive change
                "type": "number",
            },
            "archive_abs_change": {
                "description": (
                    "Minimum absolute change," " in whatever unit the attribute uses."
                ),
                # TODO: DITO
                "type": "number",
            },
            "archive_strategy": {
                "type": "string",
            },
        },
    },
}


# JSON schema for the configuration format
# Note: individual files may deviate from the schema, e.g. the db key can
# be filled in from a default. This applies to the complete configuration
# after defaults have been included.
CONFIG_SCHEMA = {
    "title": "yaml2archiving archiver configuration",
    "description": "A YAML HDB++ archiver configuration file.",
    "type": "object",
    "required": [
        "db",
        "manager",
        "archiver",
        "configuration",
    ],
    "additionalProperties": False,
    "properties": {
        "db": {
            "title": "Tango Database name",
            "description": "The TANGO_HOST to use",
            "type": "string",  # TODO TANGO_HOST, should be on format xxx.yyy.zzz:10000
        },
        "manager": {
            "title": "HDB++ manager device",
            "description": "Name of the HdbConfigurationManager device to use",
            "type": "string",  # TODO device, should be on format xxx/yyy/zzz
        },
        "archiver": {
            "title": "HDB++ archiver device",
            "description": "Name of the HdbEventSubscriber device to use",
            "type": "string",  # TODO device, should be on format xxx/yyy/zzz
        },
        "defaults": {
            "title": "Files containing defaults",
            "description": "An optional list of files containing default settings.",
            "type": "array",
            "minItems": 1,
            "uniqueitems": True,
            "items": {
                "type": "string",
            },
        },
        "configuration": {
            "title": "Archiving configuration",
            "type": "array",
            "uniqueItems": True,
            "minItems": 1,
            "items": {
                "title": "A configuration item",
                "type": "object",
                "required": ["class"],
                "additionalProperties": False,
                "properties": {
                    "class": {
                        "type": "string",
                    },
                    "filtering": {
                        "type": "object",
                        "additionalProperties": False,
                        "properties": {
                            "device": {
                                "oneOf": [
                                    {"type": "string"},
                                    {"type": "array", "items": {"type": "string"}},
                                ]
                            },
                            "server": {
                                "oneof": [
                                    {"type": "string"},
                                    {"type": "array", "items": {"type": "string"}},
                                ]
                            },
                        },
                    },
                    "attributes": ATTRIBUTES,
                },
            },
        },
    },
}


def check_config(config):
    validator = Draft7Validator(schema=CONFIG_SCHEMA)
    yield from validator.iter_errors(instance=config)


# Schema for "default" files
DEFAULTS_SCHEMA = {
    "title": "yaml2archiving defaults",
    "description": "A YAML HDB++archiver default file.",
    "type": "object",
    "properties": {
        "classes": {
            "title": "Archiving defaults",
            "type": "object",
            "uniqueItems": True,
            "minItems": 1,
            "additionalProperties": {
                "title": "A configuration item",
                "type": "object",
                "required": ["attributes"],
                "additionalProperties": False,
                "properties": {"attributes": ATTRIBUTE_DEFAULTS},
            },
        },
    },
}


def check_defaults(defaults):
    validator = Draft7Validator(schema=DEFAULTS_SCHEMA)
    yield from validator.iter_errors(instance=defaults)
