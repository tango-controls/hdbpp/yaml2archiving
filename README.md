Description
=====
This script aims to ease the configuration of the HDB++ Archiving by using YAML format files in order to add/remove/update archived attributes. Each YAML file corresponds to one HdbEventSubscriber device and can include the state of the archived attributes with their settings.

Note that the YAML files themselves are stored in a separate repo, https://gitlab.maxiv.lu.se/kits-maxiv/cfg-maxiv-yaml2archiving. This is just the tool to apply them.

Installation
====

    $ pip install .

Usage
====

yaml2archiving contains built-in help which you can get by running:

    $ yaml2archiving --help

Lets assume we have a YAML file with the following content:

```yaml
db: "g-v-csdb-0.maxiv.lu.se:10000"
manager: hdb/config/1
archiver: AUX/KITS/ARCHIVER-01

configuration:
  TangoTest:
    attributes:
      short_scalar_ro:
        archive_period: 6000
        polling_period: 3000
        archive_strategy: SERVICE
      short_scalar:
        archive_period: 7000
        polling_period: 3000
        archive_rel_change: 5
```

We also assume that we're in the appropriate control system, and that the configured "manager" (HdbConfigurationManager) and "archiver" (HdbEventSubscriber) devices are configured and running.

Executing

    $ yaml2archiving path/to/config.yaml

will generate the configuration for the polling and the archiving settings of the ```short_scalar_ro``` & ```short_scalar``` attribute of all devices of the TangoTest Class. The command may take some time, depending on the number of attributes currently configured on the archiver. The stdout result should look something like this:


```
...
CHANGE   tango://g-v-csdb-0.maxiv.lu.se:10000/sys/tg_test/3/short_scalar_ro
             archive_strategy: ALWAYS -> SERVICE
             polling_period: 2000 -> 3000
CHANGE   tango://g-v-csdb-0.maxiv.lu.se:10000/sys/tg_test/4/short_scalar_ro
             archive_strategy: ALWAYS -> SERVICE
             polling_period: 2000 -> 3000
ADD      tango://g-v-csdb-0.maxiv.lu.se:10000/sys/tg_test/1/short_scalar_ro
             archive_strategy: SERVICE
             archive_abs_change: Not specified
             archive_rel_change: Not specified
             archive_period: 6000
             polling_period: 3000
             archive_rel_change: 5
REMOVE   tango://g-v-csdb-0.maxiv.lu.se:10000/sys/tg_test1/1/short_scalar_ro	 ** Does not exist in the TangoDB (Cleanup Action)
REMOVE   tango://g-v-csdb-0.maxiv.lu.se:10000/sys/tg_test1/1/short_scalar	 ** Does not exist in the TangoDB (Cleanup Action)
...
=== No changes were made, try the '--write' option to apply. ===
```

This is a list of all the "actions" that the script would need to take in order for the archiver to be configured as described in the YAML file.

Like the last line of the output says, by default the actions are not actually applied, only listed. For that to happen, the user needs to add the `--write` option. Make sure that the actions printed are precisely the ones you expect, and then rerun the command with the `-w/--write` flag:

    $ yaml2archiving path/to/config.yaml -w

Update only
---

There is an option to only do "updates":

    $ python yaml2archiving AUX/KITS/ARCHIVER-01.yaml -u

This will have almost the same result with the command above, but will skip any ```REMOVE``` steps and will only ADD/CHANGE attributes. This might be useful when there are custom configurations present in the control system, but not yet encoded in the YAML file. However, this is not generally recommended, as the attributes might be removed in a future run without the `-u/--update` flag.


Filtering options
---
The example config above will setup archiving for *all* devices of class TangoTest in the database. In most cases this is probably what you want. For special cases, it's possible to be selective in which devices to add, by adding a `filtering` section.

```yaml
db: "g-v-csdb-0.maxiv.lu.se:10000"
manager: hdb/config/1
archiver: AUX/KITS/ARCHIVER-01

configuration:
  TangoTest:
    filtering:
      server: "TangoTest/test*"
    attributes:
      short_scalar_ro:
        archive_period: 6020
        polling_period: 3000
        archive_strategy: SERVICE
      short_scalar:
        archive_period: 7000
        polling_period: 3600

```

Note that the filter settings are combined, so only devices matching *both* the device filter and server filter will be included. Usually there is not much point in using both device and server filters though!


Filter on device name:
----
To filter on device name, specify a `device_pattern`. This uses full *regular expressions* for matching (this is different from the server pattern!).

Examples:

A single device:

    device: "sys/tg_test/test-2"

Using a simple wildcard (`.*`):

    device: "sys/tg_test/test-.*"

Get all devices named "test" or "dummy" numbered 1 or 2:

    device: "sys/tg_test/(test|dummy)-[1-2]"

If you need to specify several patterns, you can specify it as a list. This is usually much more readable than a long regex. All devices matching *any* pattern in the list will be included.

    device:
      - sys/tg_test/test-1
      - sys/tg_test/dummy-[135]

Note: regexes are often hard to get right and tricky to read. Unless you need
to match a lot of devices, or expect the list to change often, you may be better
off listing the device names one by one.

    device:
      - sys/tg_test/1
      - sys/tg_test/3
      - sys/tg_test/5


Filter on server name:
----
To filter on server name, specify a `server` filter. This uses the Tango Database API to filter. That means that the only available wildcard for servers is "`*`", matching any number of any characters.

Examples:

A single server:

    server: "TangoTest/test"

Using wildcards (`*`):

    server: "Tango*/test-*"

It's also allowed to specify a list of names/patterns:

    server:
      - TangoTest/test
      - TangoTest/other-*


Defaults
----
In order to not have to repeat a lot of configuration for the same device classes e.g. across beamlines, it's possible to rely on default settings. The configuration file may give a list of files that will be read and used to supply default values. The list is applied in order, meaning that later defaults will override earlier ones.

Defaults basically work just like if their content had been added at the top of
the file. They can also be overridden (see below).

See the configuration in the `examples/` dir for how it works.


Overriding
---
Sometimes it's necessary to configure some subset of devices differently. This can be done by subsequent configurations for the same class, which will then override previous definitions. They should therefore use filtering to specify a subset of
devices.

This method can also be used to override defaults. If you need to *remove* something by overriding, set it to `null`.

```yaml
...

  # "Base" configuration, defaults for all TangoTest devices
  - class: TangoTest
    attributes:
      ampli:
        archive_abs_change: 10
        polling_perod: 3000

  # This configuration will override the base, for the device sys/tg_test/2
  - class: TangoTest
    filtering:
      device: sys/tg_test/2
    attributes:
      ampli:
        archive_abs_change: null  # skip the abs change setting
        archive_rel_change: 3
      double_scalar:  # adds another attribute for this device
        ...

  - class: TangoTest
    filtering:
      device: sys/tg_test/3
    attributes:
      ampli: null  # Don't archive this attribute for the matching device
      boolean_scalar:
        archive_period: 10000
        pollng_period: 3000

```

Other tools
===

archiving2yaml
---

In the `tools/` directory there's a simple script called `archiving2yaml.py` that tries to generate a configuration file from an existing HDB++ archiver. Run it by giving it the device name of the archiver, and it will print the resulting file to stdout. It is not well tested and will not always give "optimal" results. It is intended to help out with the most tedious parts of writing out all the attributes and parameters. Carefully inspect the output and try applying it with `yaml2archiving` before trusting it.
