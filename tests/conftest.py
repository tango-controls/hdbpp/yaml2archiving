import logging
import os
import socket
import subprocess
from multiprocessing import Process, log_to_stderr
import sys
import time
from uuid import uuid4

import pytest
import tango

from . import TANGO_HOST
from .fake_hdb import HdbConfigurationManager, HdbEventSubscriber, DummyDevice


# Use a in memory database. If you want to debug it, change it to a filename.
PYTANGO_DATABASE_NAME = ":memory:"


@pytest.fixture(scope="function")
def db():
    """
    Runs a pytango database server that we can run the tests against.
    This eliminates the need for MySQL etc.
    """
    os.environ["TANGO_HOST"] = TANGO_HOST
    try:
        # Start our database device
        databaseds = subprocess.Popen(
            [sys.executable, "-m", "databaseds.database", "2"],
            stderr=subprocess.PIPE,
            env={
                "TANGO_HOST": TANGO_HOST,
                "PYTANGO_DATABASE_NAME": PYTANGO_DATABASE_NAME,
            },
        )

        waited = 0
        dt = 0.3
        while True:
            time.sleep(dt)
            waited += dt
            if databaseds.poll() is not None:
                stderr = databaseds.stderr.read().decode()
                print(stderr)
                raise RuntimeError(f"Database stopped: {databaseds.returncode}")
            try:
                db = tango.Database()
                db.get_info()
                break
            except tango.DevFailed as e:
                if waited > 10:
                    raise RuntimeError("Tired of waiting for database...") from e
            except AssertionError:
                pass

        yield db

    finally:
        # Clean up
        try:
            dummy.kill()
            db.delete_server(dev_info.server)
        except Exception:
            pass
        try:
            databaseds.kill()
        except Exception:
            pass

        del os.environ["TANGO_HOST"]


@pytest.fixture(scope="function")
def device(db):
    """
    Fixture that gives a Tango DB with a dummy device configured and running.
    """
    try:
        device = "test/dummy/1"
        dev_info = tango.DbDevInfo()
        dev_info.name = device
        dev_info._class = "Dummy"
        dev_info.server = "Dummy/1"
        db.add_server(dev_info.server, dev_info, with_dserver=True)

        # Start our dummy device
        dummy = subprocess.Popen(
            [sys.executable, "tests/dummy.py", "1"],
            stderr=subprocess.PIPE,
            env={
                "TANGO_HOST": TANGO_HOST,
            },
        )
        waited = 0
        dt = 0.3
        while True:
            time.sleep(dt)
            waited += dt
            if dummy.poll() is not None:
                stderr = dummy.stderr.read().decode()
                print(stderr)
                raise RuntimeError(f"Dummy device stopped: {dummy.returncode}")
            try:
                proxy = tango.DeviceProxy(
                    device, green_mode=tango.GreenMode.Synchronous
                )
                proxy.ping()
                if proxy.read_attribute("State").value == tango.DevState.RUNNING:
                    break
            except tango.DevFailed as e:
                if waited > 10:
                    raise RuntimeError("Tired of waiting for device proxy...") from e
            except AssertionError:
                pass

        yield device
    finally:
        # Clean up
        try:
            dummy.kill()
            db.delete_server(dev_info.server)
        except Exception:
            pass


@pytest.fixture(scope="function")
def fake_hdb(db):
    UUID = str(uuid4())
    MANAGER = f"yaml2archiving/manager/{UUID}"
    ARCHIVER = f"yaml2archiving/archiver/{UUID}"
    DUMMY = f"yaml2archiving/dummy/{UUID}-1"
    DUMMY2 = f"yaml2archiving/dummy/{UUID}-2"

    server = f"fake_hdb/yaml2archiving-{UUID}"
    devices = {
        MANAGER: {
            # Fake manager device that does nothing but records actions
            "class": HdbConfigurationManager,
        },
        ARCHIVER: {
            "class": HdbEventSubscriber,
            "properties": {
                "AttributeList": [
                    f"tango://{TANGO_HOST}/{DUMMY}/apa;strategy=ALWAYS",
                    f"tango://{TANGO_HOST}/{DUMMY}/cepa;strategy=ALWAYS",
                    f"tango://{TANGO_HOST}/{DUMMY}/depa;strategy=ALWAYS",
                ]
            },
        },
        DUMMY: {
            "class": DummyDevice,
            "properties": {
                "polled_attr": ["apa", "3000", "cepa", "3000", "depa", "3000"],
            },
            "attribute_properties": {
                "apa": {"archive_rel_change": 47},
                "bepa": {"archive_abs_change": 2},
            },
        },
        DUMMY2: {
            "class": DummyDevice,
            "properties": {
                "polled_attr": ["apa", "3000", "cepa", "3000", "depa", "3000"],
            },
            "attribute_properties": {
                "apa": {"archive_rel_change": 47},
                "bepa": {"archive_abs_change": 2},
            },
        },
    }

    try:

        devinfos = []
        devclasses = set()
        for device, config in devices.items():
            devinfo = tango.DbDevInfo()
            devinfo.name = device
            devinfo._class = config["class"].__name__
            devinfo.server = server
            devinfos.append(devinfo)
            devclasses.add(config["class"])
        db.add_server(devinfo.server, devinfos, with_dserver=True)
        for device, config in devices.items():
            props = config.get("properties")
            if props:
                db.put_device_property(device, props)
            attr_props = config.get("attribute_properties")
            if attr_props:
                db.put_device_attribute_property(device, attr_props)

        srv = subprocess.Popen(
            [sys.executable, "tests/fake_hdb.py", f"yaml2archiving-{UUID}"],
            stderr=subprocess.PIPE,
            env={
                "TANGO_HOST": TANGO_HOST,
            },
        )

        # log_to_stderr()
        # proc = Process(
        #     target=tango.server.run,
        #     kwargs={
        #         "classes": tuple(devclasses),
        #         "args": server.split("/"),
        #         "green_mode": tango.GreenMode.Synchronous,
        #     },
        #     #daemon=False,
        # )

        # proc.start()
        proxies = [tango.DeviceProxy(device) for device in devices]
        tries = 0
        for proxy in proxies:
            while True:
                if srv.poll() is not None:
                    stderr = srv.stderr.read().decode()
                    print(stderr)
                    raise RuntimeError(f"Database stopped: {srv.returncode}")
                try:
                    proxy.ping()
                    break
                except tango.DevFailed:
                    time.sleep(0.1)
                tries += 1
                if tries == 30:
                    raise RuntimeError(f"Failed to start {proxy.dev_name()}")

        yield proxies, (TANGO_HOST, MANAGER, ARCHIVER, DUMMY, DUMMY2)
    finally:
        # Clean up
        try:
            srv.kill()
            db.delete_server(devinfo.server)
        except Exception:
            pass
