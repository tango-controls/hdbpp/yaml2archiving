from unittest.mock import Mock

import pytest
import tango
from yaml2archiving.archiver import get_current_attributes

from . import attr_config_in_db, TANGO_HOST


def test_get_current_attributes_basic(device):
    mock_device_proxy = Mock(spec=tango.DeviceProxy)
    attribute = f"tango://{TANGO_HOST}/{device}/bananas"
    props = {
        "AttributeList": [attribute]
    }
    mock_device_proxy.get_property.return_value = props
    mock_attribute_proxy = Mock(spec=tango.AttributeProxy)
    # mock_attribute_config = Mock(spec=tango.AttributeInfoEx)
    # mock_event_info = Mock(spec=tango.AttributeEventInfo)
    # mock_event_info.archive_rel_change = 3
    # mock_event_info.archive_abs_change = 0
    # mock_event_info.archive_period = 9182
    # mock_attribute_config.events.arch_event = mock_event_info
    # mock_attribute_proxy.get_poll_period.return_value = 2000
    with attr_config_in_db((
            device, "bananas", {
                "archive_rel_change": 3,
                "archive_period": 9182,
                "polling_period": 2000
            })):

        # mock_attribute_proxy.get_config.return_value = mock_attribute_config
        archived = get_current_attributes(
            "my/great/archiver",
            get_device_proxy=lambda dev: mock_device_proxy,
            # get_attribute_proxy=lambda dev: mock_attribute_proxy,
        )

    assert len(archived) == 1
    assert archived[attribute] == {
        "archive_rel_change": 3,
        "archive_period": 9182,
        "polling_period": 2000
    }

    # # Clean up
    # db.delete_device_attribute_property(device, {"banana": ["archive_rel_change", "archive_period"]})
    # db.delete_device_property(device, "polled_attr")


def test_get_current_attributes_broken_archiver():
    mock_device_proxy = Mock(spec=tango.DeviceProxy)
    mock_device_proxy.get_property.side_effect = tango.DevFailed(Mock(desc="hello"))

    with pytest.raises(RuntimeError):
        get_current_attributes(
            "my/great/archiver",
            get_device_proxy=lambda dev: mock_device_proxy,
        )


# def test_get_current_attributes_broken_attribute():
#     device = "test/dev/189"
#     mock_device_proxy = Mock(spec=tango.DeviceProxy)
#     attribute = f"tango://some.tango.db:10000/{device}/bananas"
#     props = {
#         "AttributeList": [attribute]
#     }
#     mock_device_proxy.get_property.return_value = props
#     # mock_attribute_proxy = Mock(spec=tango.AttributeProxy)
#     # mock_attribute_config = Mock(spec=tango.AttributeInfoEx)
#     # mock_event_info = Mock(spec=tango.AttributeEventInfo)
#     # mock_event_info.archive_rel_change = "häst"
#     # mock_event_info.archive_abs_change = 0
#     # mock_event_info.archive_period = 9182
#     # mock_attribute_config.events.arch_event = mock_event_info
#     # mock_attribute_proxy.get_poll_period.return_value = 2000

#     # mock_attribute_proxy.get_config.return_value = mock_attribute_config

#     db = tango.Database()
#     db.put_device_attribute_property(device, {
#         "bananas": {
#             "archive_rel_change": "häst",
#             "archive_period": 9182,
#         }
#     })
#     db.put_device_property(device, {"polled_attr": ["bananas", "2000"]})


#     def get_attribute_proxy(attr):
#         error_mock = Mock(desc="Some desc")
#         raise tango.DevFailed(error_mock)

#     archived, broken = get_current_attributes(
#         "my/great/archiver",
#         get_device_proxy=lambda dev: mock_device_proxy,
#         get_attribute_proxy=get_attribute_proxy
#     )

#     assert len(archived) == 1
#     # assert archived[attribute] == {}
#     assert len(broken) == 1
#     assert attribute in broken

#     # Clean up
#     db.delete_device_attribute_property(device, {"banana": ["archive_rel_change", "archive_period"]})
#     db.delete_device_property(device, "polled_attr")
