from contextlib import contextmanager

import pytest
import tango  # type: ignore


# Database settings
# TODO get a free port?
TANGO_HOST = f"localhost:12000"


@contextmanager
def attr_config_in_db(*configs):
    """
    Temporarily configure archiving settings for some attributes
    in the Tango DB.
    Recommended to use a unique device name to avoid collisions with
    existing stuff, other tests...
    """
    db = tango.Database()
    device_attr_props = {}
    device_props = {}
    for device, attr, config in configs:
        attr_props = {}
        for prop in [
                "archive_rel_change",
                "archive_abs_change",
                "archive_period"
        ]:
            if prop in config:
                attr_props[prop] = config[prop]
        device_attr_props.setdefault(device, {})[attr] = attr_props
        if "polling_period" in config:
            device_props.setdefault(device, {}).setdefault("polled_attr", []).extend([attr, config["polling_period"]])
    for device, props in device_attr_props.items():
        print(f"Configuring attribute props for {device}: {props}")
        db.put_device_attribute_property(device, props)
    for device, props in device_props.items():
        print(f"Configuring props for {device}: {props}")
        db.put_device_property(device, props)

    yield

    # Cleaning up
    for device, props in device_attr_props.items():
        db.delete_device_attribute_property(device, props)
    for device, props in device_props.items():
        db.delete_device_property(device, props)
