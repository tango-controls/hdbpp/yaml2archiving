from collections import defaultdict
import json
from random import random

from tango.server import Device, attribute, command, device_property, run


class HdbConfigurationManager(Device):

    """
    A device that behaves like a HDB++ manager, but in fact does nothing
    except record how it is being used.
    Only supports the functionality used by yaml2archiving.
    """

    def init_device(self):
        self._reset()
        # Store history of changes, for use in test asserts
        self.added = []
        self.removed = []
        self.stopped = []

    def _reset(self):
        self.settings = {}

    # The "Set*" attributes are used when adding a new attribute,
    # by writing the corresponding attributes for the desired settings,
    # and then running "AttributeAdd". The default values are the
    # original "unset" values in the real HDB++ manager.
    @attribute(dtype=str)
    def SetArchiver(self):
        return self.settings.get("archiver", "")

    @SetArchiver.write
    def write_SetArchiver(self, value):
        self.settings["archiver"] = value

    @attribute(dtype=str)
    def SetAttributeName(self):
        return self.settings.get("attribute", "")

    @SetAttributeName.write
    def write_SetAttributeName(self, value):
        self.settings["attribute"] = value

    @attribute(dtype=float)
    def SetAbsoluteEvent(self):
        return self.settings.get("archive_abs_change", -1.0)

    @SetAbsoluteEvent.write
    def write_SetAbsoluteEvent(self, value):
        self.settings["archive_abs_change"] = value

    @attribute(dtype=float)
    def SetRelativeEvent(self):
        return self.settings.get("archive_rel_change", -1.0)

    @SetRelativeEvent.write
    def write_SetRelativeEvent(self, value):
        self.settings["archive_rel_change"] = value

    @attribute(dtype=int)
    def SetPeriodEvent(self):
        return self.settings.get("archive_period", -1)

    @SetPeriodEvent.write
    def write_SetPeriodEvent(self, value):
        self.settings["archive_period"] = value

    @attribute(dtype=int)
    def SetPollingPeriod(self):
        return self.settings.get("polling_period", -1)

    @SetPollingPeriod.write
    def write_SetPollingPeriod(self, value):
        self.settings["polling_period"] = value

    @attribute(dtype=bool)
    def SetCodePushedEvent(self):
        return self.settings.get("code_pushed_event", False)

    @SetCodePushedEvent.write
    def write_SetCodePushedEvent(self, value):
        self.settings["code_pushed_event"] = value

    @attribute(dtype=str)
    def SetStrategy(self):
        return self.settings.get("archive_strategy", "")

    @SetStrategy.write
    def write_SetStrategy(self, value):
        self.settings["archive_strategy"] = value

    @attribute(dtype=str)
    def SetTTL(self):
        return self.settings.get("ttl", 0)

    @SetTTL.write
    def write_SetTTL(self, value):
        self.settings["ttl"] = value

    @command
    def AttributeAdd(self):
        self.added.append(self.settings)
        self._reset()

    @command(dtype_out=str)
    def get_added(self):
        """ Test convenience command, not part of real HDB++ device """
        return json.dumps(self.added)

    @command(dtype_in=str)
    def AttributeRemove(self, attr):
        self.removed.append(attr)

    @command(dtype_out=[str])
    def get_removed(self):
        """ Test convenience command, not part of real HDB++ device """
        return self.removed

    @command(dtype_in=str)
    def AttributeStop(self, attr):
        self.stopped.append(attr)

    @command(dtype_out=[str])
    def get_stopped(self):
        """ Test convenience command, not part of real HDB++ device """
        return self.stopped


class HdbEventSubscriber(Device):

    """
    A dummy archiver device
    """

    attributelist = device_property(dtype=[str])

    @attribute(dtype=[str], max_dim_x=100)
    def AttributeList(self):
        return self.attributelist


class DummyDevice(Device):
    """
    A dummy device, to be fake archived
    """
    @attribute(dtype=float)
    def Apa(self):
        return random()

    @attribute(dtype=float)
    def bEpa(self):
        return random()

    @attribute(dtype=float)
    def cePa(self):
        return random()

    @attribute(dtype=float)
    def depA(self):
        return random()


if __name__ == "__main__":
    run((HdbConfigurationManager, HdbEventSubscriber, DummyDevice))
