import tango
from yaml2archiving import get_attribute_configs

from . import attr_config_in_db, TANGO_HOST


def test_get_attribute_configs(device):
    attrs = [
        f"tango://{TANGO_HOST}/{device}/long_scalar",
        f"tango://{TANGO_HOST}/{device}/short_scalar",
        f"tango://{TANGO_HOST}/{device}/nonexistent_scalar",
    ]
    with attr_config_in_db(
            (
                device, "long_scalar", {
                    "archive_abs_change": 14.4,
                    "archive_period": 10000,
                    "polling_period": 1234
                }
            ), (
                device, "short_scalar", {
                    "archive_rel_change": 3,
                }
            )):
        db = tango.Database()
        configs = get_attribute_configs(db, attrs)

    assert configs[attrs[0]] == {
        "archive_abs_change": 14.4,
        "archive_period": 10000,
        "polling_period": 1234
    }
    assert configs[attrs[1]] == {
        "archive_rel_change": 3,
    }
    assert configs[attrs[2]] == {}



def test_get_attribute_configs_inconsistent_casing(device):
    attrs = [
        f"tango://{TANGO_HOST}/{device}/long_scalar",
        f"tango://{TANGO_HOST}/{device}/short_scalar",
        f"tango://{TANGO_HOST}/{device}/nonexistent_scalar",
    ]
    with attr_config_in_db(
            (
                device, "LONG_scalar", {
                    "archive_abs_change": 14.4,
                    "archive_period": 10000,
                    "polling_period": 1234
                }
            ), (
                device, "ShOrT_ScAlAr", {
                    "archive_rel_change": 3,
                }
            )):
        db = tango.Database()
        configs = get_attribute_configs(db, attrs)

    assert configs[attrs[0]] == {
        "archive_abs_change": 14.4,
        "archive_period": 10000,
        "polling_period": 1234
    }
    assert configs[attrs[1]] == {
        "archive_rel_change": 3,
    }
    assert configs[attrs[2]] == {}
