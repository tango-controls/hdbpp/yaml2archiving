import fnmatch
from tempfile import TemporaryDirectory
from pathlib import Path
import re
from unittest.mock import Mock

import pytest
import tango

from yaml2archiving.config import (log_validation_errors, merge_defaults, load_configuration,
                                   get_desired_attributes, get_class_devices)


def test_merge_defaults_no_default():

    config = {
        "class": "TestDevice",
        "attributes": {
            "potato": {
                "polling_period": 4000,
                "archive_period": 10000,
            }
        }
    }

    defaults = {}

    result = merge_defaults(config, defaults)

    # Config unchanged
    assert result == config
    assert len(result["attributes"].keys()) == 1
    assert "potato" in result["attributes"]


def test_merge_defaults_use_defaults():

    config = {
        "class": "TestDevice",
    }

    defaults = {
        "TestDevice": {
            "attributes": {
                "apple": {
                    "polling_period": 500,
                    "archive_period": 10000,
                },
            }
        }
    }

    result = merge_defaults(config, defaults)

    # Use default if there's nothing in the config
    assert result["attributes"]["apple"] == {
        "polling_period": 500,
        "archive_period": 10000
    }
    assert len(result["attributes"].keys()) == 1


def test_merge_defaults_override_default():

    defaults = {
        "TestDevice": {
            "attributes": {
                "banana": {
                    "polling_period": 3000,
                    "archive_rel_change": 2,
                },
            }
        }
    }

    config = {
        "class": "TestDevice",
        "attributes": {
            "banana": {
                "archive_rel_change": 3
            },
        }
    }

    result = merge_defaults(config, defaults)

    # default overridden
    assert result["attributes"]["banana"] == {
        "polling_period": 3000,
        "archive_rel_change": 3,
    }

    assert len(result["attributes"].keys()) == 1


def test_merge_defaults_override_default_ignores_case():

    defaults = {
        "TestDevicE": {
            "attributes": {
                "banana": {
                    "polling_period": 3000,
                    "archive_rel_change": 2,
                },
            }
        }
    }

    config = {
        "class": "TestDevice",
        "attributes": {
            "BaNaNa": {
                "archive_rel_change": 3
            },
        }
    }

    result = merge_defaults(config, defaults)

    # default overridden
    assert result["attributes"]["banana"] == {
        "polling_period": 3000,
        "archive_rel_change": 3,
    }

    assert len(result["attributes"].keys()) == 1


def test_merge_defaults_skip_attribute():

    defaults = {
        "TestDevice": {
            "attributes": {
                "orange": {
                    "polling_period": 500,
                    "code_push_events": True,
                },
            }
        }
    }

    config = {
        "class": "TestDevice",
        "attributes": {
            "orange": None,  #
        }
    }

    result = merge_defaults(config, defaults)

    assert "orange" not in result["attributes"]
    assert len(result["attributes"].keys()) == 0


def test_merge_defaults_skip_parameter():

    defaults = {
        "TestDevice": {
            "attributes": {
                "papaya": {
                    "polling_period": 1000,
                    "archive_abs_change": 2,
                }
            }
        }
    }

    config = {
        "class": "TestDevice",
        "attributes": {
            "papaya": {
                "polling_period": 1000,
                "archive_abs_change": None,
                "archive_rel_change": 6,
            },
        }
    }

    result = merge_defaults(config, defaults)

    # Remove default parameter by setting it to None
    assert result["attributes"]["papaya"] == {
        "polling_period": 1000,
        "archive_rel_change": 6
        # No abs_rel_change even though it's in the defaults
    }
    assert len(result["attributes"].keys()) == 1


DB = "my.tango.host:10000"
MANAGER = "yaml2archiving/manager/1"
ARCHIVER = "yaml2archiving/archiver/1"
DUMMY = "yaml2archiving/dummy/1"


def test_load_configuration():

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with configfile.open("w") as f:
            f.write(
                "\n".join(
                    [
                        f"db: {DB}",
                        f"manager: {MANAGER}",
                        f"archiver: {ARCHIVER}",
                        "configuration:",
                        "  - class: DummyDevice",
                        "    attributes:",
                        "      apa:",
                        "        polling_period: 3000",
                        "        archive_rel_change: 1",
                        "      bepa:",
                        "        polling_period: 2000",
                        "      cepa:",
                        "        archive_rel_change: 1",
                        "      depa:",
                        "        polling_period: 4000",
                    ]
                )
            )

        config = load_configuration(str(configfile))
    assert config["configuration"][0]["class"] == "DummyDevice"
    assert config["configuration"][0]["attributes"] == {
        "apa": {
            "polling_period": 3000,
            "archive_rel_change": 1,
        },
        "bepa": {
            "polling_period": 2000,
        },
        "cepa": {
            "archive_rel_change": 1,
        },
        "depa": {
            "polling_period": 4000,
        }
    }


def test_load_configuration_defaults():

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with configfile.open("w") as f:
            f.write(
                "\n".join(
                    [
                        f"db: {DB}",
                        f"manager: {MANAGER}",
                        f"archiver: {ARCHIVER}",
                        "defaults:",
                        "  - ../defaults.yaml",
                        "configuration:",
                        "  - class: DummyDevice",
                        "    filtering:",
                        "      server: DummyDevice/hej",
                    ]
                )
            )

        defaultfile = subdir1 / "defaults.yaml"
        with defaultfile.open("w") as f:
            f.write(
                "\n".join(
                    [
                        "classes:",
                        "  DummyDevice:",
                        "    attributes:",
                        "      apa:",
                        "        polling_period: 3000",
                        "        archive_rel_change: 1",
                        "      bepa:",
                        "        polling_period: 2000",
                        "      cepa:",
                        "        archive_rel_change: 1",
                        "      depa:",
                        "        polling_period: 4000",
                    ]
                )
            )

        config = load_configuration(str(configfile))

    assert len(config["configuration"]) == 1
    dummy = config["configuration"][0]
    assert dummy["class"] == "DummyDevice"
    assert dummy["filtering"] == {"server": ["DummyDevice/hej"]}
    assert dummy["attributes"] == {
        "apa": {
            "polling_period": 3000,
            "archive_rel_change": 1,
        },
        "bepa": {
            "polling_period": 2000,
        },
        "cepa": {
            "archive_rel_change": 1,
        },
        "depa": {
            "polling_period": 4000,
        }
    }


def test_load_configuration_defaults_override():

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        defaultfile = subdir1 / "defaults.yaml"
        with defaultfile.open("w") as f:
            f.write(
                "\n".join(
                    [
                        "classes:",
                        "  DummyDevice:",
                        "    attributes:",
                        "      apa:",
                        "        polling_period: 3000",
                        "        archive_rel_change: 1",
                        "      bepa:",
                        "        polling_period: 2000",
                        "      cepa:",
                        "        archive_rel_change: 1",
                        "      depa:",
                        "        polling_period: 4000",
                    ]
                )
            )

        configfile = subdir2 / "archiver.yaml"
        with configfile.open("w") as f:
            f.write(
                "\n".join(
                    [
                        f"db: {DB}",
                        f"manager: {MANAGER}",
                        f"archiver: {ARCHIVER}",
                        "defaults:",
                        "  - ../defaults.yaml",
                        "configuration:",
                        "  - class: DummyDevice",
                        "    attributes:",
                        "      apa:",
                        "        archive_rel_change: null",  # this should skip the setting
                        "        archive_abs_change: 5",
                        "      bepa: null",  # this should skip the attribute
                    ]
                )
            )

        config = load_configuration(str(configfile))

    assert config["configuration"][0]["class"] == "DummyDevice"
    assert config["configuration"][0]["attributes"] == {
        "apa": {
            "polling_period": 3000,
            # "archive_rel_change": 1, skipped by setting to null
            "archive_abs_change": 5,  # Default overridden
        },
        # # Default attribute skipped by setting to null
        # "bepa": {
        #     "polling_period": 2000,
        # },
        "cepa": {
            "archive_rel_change": 1,
        },
        "depa": {
            "polling_period": 4000,
        }
    }


def test_load_configuration_bad_yaml():

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with configfile.open("w") as f:
            f.write("""
abc: 1
def 56
            """)

        with pytest.raises(RuntimeError):
            load_configuration(str(configfile))


def test_load_configuration_bad_filter(caplog):

    bad_regex = "092092((***"

    with TemporaryDirectory() as tmp:

        p = Path(tmp)
        subdir1 = p / "subdir1"
        subdir1.mkdir()
        subdir2 = subdir1 / "subdir2"
        subdir2.mkdir()

        configfile = subdir2 / "archiver.yaml"
        with configfile.open("w") as f:
            f.write(f"""
db: a.b.c:10000
manager: my/manager/here
archiver: my/archiver/here
configuration:
  - class: DummyDevice
    filtering:
      device: "{bad_regex}"
    attributes:
      apa:
        archive_rel_change: 4.4
            """)

        with pytest.raises(RuntimeError):
            load_configuration(str(configfile))
        record, = caplog.records
        assert bad_regex in record.message


def test_get_desired_attributes():

    configs = [
        {
            "class": "TestClass",
            "attributes": {
                "apple": {
                    "polling_period": 1000,
                    "archive_period": 10000,
                },
                "banana": {
                    "polling_period": 2000,
                    "archive_period": 30000,
                }
            }
        }
    ]

    TANGO_HOST = "some.fun.host:10000"
    DEVICES = "test/dev/1", "test/fish/2", "test/dev/3", "test/fish/4"
    MOCK_DB = Mock(spec=tango.Database)

    MOCK_DB.get_device_name.return_value = DEVICES

    attributes = get_desired_attributes(TANGO_HOST, configs, db=MOCK_DB)

    assert len(attributes) == 4 * 2
    for device in DEVICES:
        for i, attribute in enumerate(["apple", "banana"]):
            assert f"tango://{TANGO_HOST}/{device}/{attribute}" in attributes


def test_get_desired_attributes_server_filter():

    configs = [
        {
            "class": "TestClass",
            "filtering": {
                "server": [
                    "Test/1",
                ]
            },
            "attributes": {
                "apple": {
                    "polling_period": 1000,
                    "archive_period": 10000,
                },
                "banana": {
                    "polling_period": 2000,
                    "archive_period": 30000,
                }
            }
        }
    ]

    TANGO_HOST = "some.fun.host:10000"
    DEVICES = "test/dev/1", "test/fish/2", "test/dev/3", "test/fish/4"
    MOCK_DB = Mock(spec=tango.Database)

    results = {
        ("Test/1", "TestClass"): DEVICES[:2],
    }

    MOCK_DB.get_device_name.side_effect = lambda *args: results[args]

    attributes = get_desired_attributes(TANGO_HOST, configs, db=MOCK_DB)

    assert len(attributes) == 2 * 2
    for device in DEVICES[:2]:
        for i, attribute in enumerate(["apple", "banana"]):
            assert f"tango://{TANGO_HOST}/{device}/{attribute}" in attributes


def test_get_desired_attributes_device_filter():

    configs = [
        {
            "class": "TestClass",
            "filtering": {
                "device": [
                    "test/fish/.*",
                ]
            },
            "attributes": {
                "apple": {
                    "polling_period": 1000,
                    "archive_period": 10000,
                },
                "banana": {
                    "polling_period": 2000,
                    "archive_period": 30000,
                }
            }
        }
    ]

    TANGO_HOST = "some.fun.host:10000"
    DEVICES = "test/dev/1", "test/fish/2", "test/dev/3", "test/fish/4"
    MOCK_DB = Mock(spec=tango.Database)

    MOCK_DB.get_device_name.return_value = DEVICES

    attributes = get_desired_attributes(TANGO_HOST, configs, db=MOCK_DB)

    assert len(attributes) == 2 * 2
    for device in ["test/fish/2", "test/fish/4"]:
        for i, attribute in enumerate(["apple", "banana"]):
            assert f"tango://{TANGO_HOST}/{device}/{attribute}" in attributes


def test_get_desired_attributes_device_filter_requires_full_match():

    configs = [
        {
            "class": "TestClass",
            "filtering": {
                "device": [
                    "test/.*/1",
                ]
            },
            "attributes": {
                "apple": {
                    "polling_period": 1000,
                    "archive_period": 10000,
                },
            }
        }
    ]

    TANGO_HOST = "some.fun.host:10000"
    DEVICES = [
        "test/fish/1",  # Matched
        "test/chips/1",  # Matched
        "test/fish/2",  # Not matched
        "test/fish/17"  # Not matched even though prefix matches
    ]
    MOCK_DB = Mock(spec=tango.Database)

    MOCK_DB.get_device_name.return_value = DEVICES

    attributes = get_desired_attributes(TANGO_HOST, configs, db=MOCK_DB)

    assert len(attributes) == 2
    assert f"tango://{TANGO_HOST}/test/fish/1/apple" in attributes
    assert f"tango://{TANGO_HOST}/test/chips/1/apple" in attributes


def test_log_validation_errors(caplog):
    error1 = Mock(message="Something bad", path=("some", "path", "here",))
    error2 = Mock(message="Something else", path=("some", "other", "path", "here",))

    filename = "some_file.yml"

    log_validation_errors(filename, [error1, error2])

    assert len(caplog.records) == 2
    for record in caplog.records:
        assert record.levelname == "ERROR"
        assert filename in record.message


DBDATA = {
    "Server/1": {
        "SomeClass": ["nice/device/1", "nice/device/2", "silly/device/2"],
    },
    "Server/2": {
        "OtherClass": ["other/device/1", "other/device/2"],
    }
}


def fake_get_device_name(srv, clss):
    regex = fnmatch.translate(srv)
    devices = []
    for srv, data in DBDATA.items():
        if re.match(regex, srv, flags=re.IGNORECASE):
            devices.extend(data.get(clss, []))
    return devices


def test_get_class_devices(caplog):
    fake_db = Mock()
    fake_db.get_device_name = fake_get_device_name
    devices = get_class_devices(fake_db, "SomeClass", "nice/.*")
    assert devices == ["nice/device/1", "nice/device/2"]


def test_get_class_devices_server_list():
    fake_db = Mock()
    fake_db.get_device_name = fake_get_device_name
    devices = get_class_devices(fake_db, "OtherClass", ".*", ["Server/1", "Server/2"])
    assert devices == ["other/device/1", "other/device/2"]


def test_get_class_devices_device_list():
    fake_db = Mock()
    fake_db.get_device_name = fake_get_device_name
    devices = get_class_devices(fake_db, "SomeClass",
                                ["nice/device/1", "silly/device/2", "what/is/this"],
                                "Server/1")
    assert devices == ["nice/device/1", "silly/device/2"]


def test_get_class_device_no_db_results(caplog):
    fake_db = Mock()
    fake_db.get_device_name = fake_get_device_name

    devices = get_class_devices(fake_db, "MisspelledClass", "nice/.*")
    assert devices == []
    record, = caplog.records
    assert record.levelname == "WARNING"
    assert "MisspelledClass" in record.message


def test_get_class_device_no_filter_results(caplog):
    fake_db = Mock()
    fake_db.get_device_name = fake_get_device_name

    bad_pattern = "misspelled/.*"
    devices = get_class_devices(fake_db, "SomeClass", bad_pattern)
    assert devices == []
    record, = caplog.records
    assert record.levelname == "WARNING"
    assert bad_pattern in record.message
